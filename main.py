# Merge custom gymtron databases in to a single one
# Used for when multiple gyms decide they want to be run under a single system

# TODO: the various main flows (users, events, roombookings)
# would be better off being separated in to individual classes in different files
# this would make the whole thing a lot easier to maintain

import sys
import json
import argparse
import traceback
import configparser
import pymysql as mysql

def main():
    """ Start the program """

    # load config
    configuration = load_config()

    # which database do we want
    which = input("Which database (by venue_id)?: ")
    which = int(which) if which.isdigit() else None

    # if valid
    # TODO: check against venue_ids
    if which == 1 or which == 2:
        start_merge(configuration, which)
    else:
        print('Invalid database')
        sys.exit()


def load_config():
    """ loads the settings files from config.ini """

    config = configparser.ConfigParser()
    config.read('config.ini')

    if not "databases" in config:
        print('Config file missing information')
        sys.exit()

    return config


def start_merge(config, which_database):
    """ Begins the merge. Sets up a database connection and
        calls functions for specific tasks until merge complete """

    try:

        # sets up the host/port and databases
        host = config['databases']['host']
        port = int(config['databases']['port'])

        # set up receiving master database
        master = mysql.connect(host=host,
                               port=port,
                               user=config['databases']['master_database_user'],
                               password=config['databases']['master_database_pass'],
                               db=config['databases']['master_database'],
                               cursorclass=mysql.cursors.DictCursor)

        # set up to merge database
        database = config['databases']['merge_database_1']
        user = config['databases']['merge_database_1_user']
        password = config['databases']['merge_database_1_pass']
        if which_database == 2:
            database = config['databases']['merge_database_2']
            user = config['databases']['merge_database_2_user']
            password = config['databases']['merge_database_2_pass']

        to_merge = mysql.connect(host=host,
                                 port=port,
                                 user=user,
                                 password=password,
                                 db=database,
                                 cursorclass=mysql.cursors.DictCursor)

    except Exception as exception:
        print("Could not connect to databases: " + str(exception))
        sys.exit()

    # find any cli arguments    
    parser = argparse.ArgumentParser()
    parser.add_argument("--reset", help="Reset master database tables", action="store_true")
    args = parser.parse_args()
    if args.reset:
        reset_tables(master)

    # once we're here, start calling functions to do their tasks one by one
    merge_shop(master, to_merge, which_database)
    subscriptions = merge_users(master, to_merge, which_database)
    merge_events(master, to_merge, which_database, subscriptions)
    merge_roombookings(master, to_merge, which_database, subscriptions)

    # close database connections
    master.close()
    to_merge.close()


def reset_tables(master):
    """ Reset tables and autoincrements in master database """
    with master.cursor() as cursor:

        print("Resetting master database")

        tables = [
            'mono_company_subscriptions_categories',
            'mono_events_levels',
            'mono_events_special_bookings',
            'mono_events_waitinglist_archive',
            'mono_events_waitinglist',
            'mono_events_registrants_archive',
            'mono_events_registrants',
            'mono_events',
            'mono_roombooking_exclusions',
            'mono_roombooking_rooms_levels',
            'mono_roombooking_bookings',
            'mono_roombooking_rooms',
            'mono_shop_till_sales',
            'mono_subscriptions_actions',
            'mono_subscriptions_cancellations',
            'mono_subscriptions_custom_field_map',
            'mono_subscriptions_ref_history',
            'mono_subscriptions_transactions',
            'mono_subscriptions',
            'users'
        ]

        for table in tables:
            cursor.execute("DELETE FROM %s" % table)
            cursor.execute("ALTER TABLE %s AUTO_INCREMENT = 1" % table)

        # reset system files
        cursor.execute("DELETE FROM system_files WHERE attachment_type = 'Monologophobia\\Subscriptions\\Models\\Subscription'")

        master.commit()


def merge_shop(master, to_merge, venue_id):
    """ Merge all shop tables. Fortunately at this point we just need till sales """
    try:
        # read records from to_merge
        with to_merge.cursor() as cursor:

            print("Reading Till Items")

            cursor.execute("SELECT * from mono_shop_till_sales")
            products = cursor.fetchall()

            # remove ID as we just want a straight insert
            # also add venue_id
            for element in products:
                del element['id']
                element['venue_id'] = venue_id

        # insert into master
        with master.cursor() as cursor:

            print("Writing Till Items")

            # set up columns and variables, and inserts
            insert_many(cursor, 'mono_shop_till_sales', products)

            master.commit()

    except Exception as exception:
        print("merge_shop error: " + str(exception))


def merge_users(master, to_merge, which_database):
    """ Merge customer accounts """
    try:

        # get from old
        with to_merge.cursor() as cursor:

            print("Reading Customers")

            # get users
            cursor.execute("SELECT * FROM users")
            users = cursor.fetchall()
            # get subscriptions
            cursor.execute("SELECT * FROM mono_subscriptions")
            subscriptions = cursor.fetchall()
            # get subscriptions transactions
            cursor.execute("SELECT * FROM mono_subscriptions_transactions")
            transactions = cursor.fetchall()
            # get subscriptions reference history
            cursor.execute("SELECT * FROM mono_subscriptions_ref_history")
            ref_history = cursor.fetchall()
            # get custom field map
            cursor.execute("SELECT * FROM mono_subscriptions_custom_field_map")
            custom_field_map = cursor.fetchall()
            # get cancellation data
            cursor.execute("SELECT * FROM mono_subscriptions_cancellations")
            cancellations = cursor.fetchall()
            # get actions
            cursor.execute("SELECT * FROM mono_subscriptions_actions")
            actions = cursor.fetchall()
            # get marketing preferences
            cursor.execute("SELECT * FROM mono_company_subscriptions_categories")
            marketing_preferences = cursor.fetchall()
            # get subscription images
            # wat
            cursor.execute("SELECT * FROM system_files WHERE attachment_type = '%s'" % "Monologophobia\\\\Subscriptions\\\\Models\\\\Subscription")
            system_files = cursor.fetchall()

        # put in to new
        with master.cursor() as cursor:

            print("Inserting users")
            users = handle_users(cursor, users)

            if users and subscriptions:
                print("Inserting subscriptions")
                subscriptions = handle_subscriptions(cursor, subscriptions, users, which_database)

            if subscriptions and cancellations:
                print("Inserting cancellations records")
                handle_cancellation_reports(cursor, subscriptions, cancellations, which_database)

            if subscriptions and custom_field_map:
                print("Inserting Custom Fields mappings")
                handle_custom_field_mapping(cursor, subscriptions, custom_field_map, which_database)

            if subscriptions and ref_history:
                print("Inserting transactions, references, actions, and marketing preferences")
                handle_subscriptions_extras(cursor, subscriptions, transactions, ref_history, actions, marketing_preferences)

            if subscriptions and system_files:
                print("Copying image references")
                for image in system_files:
                    del image['id']
                    for subscription in subscriptions:
                        if image['attachment_id'] == subscription['id']:
                            image['attachment_id'] = subscription['new_id']
                            break
                insert_many(cursor, 'system_files', system_files)

            master.commit()

            return subscriptions

    except Exception as exception:
        print("Merge Users error: " + str(exception))
        traceback.print_exc()
        return False


def handle_users(cursor, users):
    """ handles the users table migration """

    # get all existing users
    cursor.execute("SELECT * FROM users")
    existing_users = cursor.fetchall()

    # remove IDs and do nothing with duplicates
    temp_users = []
    for user in users:
        # avoid cloning references
        u = user.copy()
        exists = False
        del u['id']
        # if the user already exists
        for existing_user in existing_users:
            if existing_user['email'].lower() == u['email'].lower():
                print("User already exists: " + str(u['email']))
                exists = True
                break
        if not exists:
            temp_users.append(u)

    insert_many(cursor, 'users', temp_users)

    cursor.execute("SELECT * FROM users")
    new_users = cursor.fetchall()

    # add the new user ids to the original user object
    for user in users:
        for new_user in new_users:
            if user['email'].lower() == new_user['email'].lower():
                user['new_id'] = new_user['id']
                break

    return users


def handle_subscriptions(cursor, subscriptions, users, venue_id):
    """ Handle subscriptions merging """

    # get existing subscriptions
    cursor.execute("SELECT * FROM mono_subscriptions")
    existing_subscriptions = cursor.fetchall()

    # update all subscriptions to new user_ids and remove existing ids
    temp_subscriptions = []
    for subscription in subscriptions:
        sub = subscription.copy()
        del sub['id']
        for user in users:
            if user['id'] == sub['user_id']:
                sub['user_id'] = user['new_id']
                break
        # check for already existing and handle offline
        exists = False
        for existing_subscription in existing_subscriptions:
            if (existing_subscription['email'].lower() == sub['email'].lower()):
                # how do we handle duplicates? If PAYG or MoveGB, ignore safely
                bad_duplicate = False
                if venue_id == 1:
                    if sub['level_id'] not in [3, 4, 17]:
                        bad_duplicate = True
                elif venue_id == 2:
                    if sub['level_id'] not in [3, 4, 9, 23, 34, 40]:
                        bad_duplicate = True
                if bad_duplicate:
                    print("Duplicate subscription: " + str(existing_subscription['email']))
                exists = True
                break
        if not exists:
            sub['level_id'] = handle_level_changes(sub, venue_id)
            temp_subscriptions.append(sub)

    insert_many(cursor, 'mono_subscriptions', temp_subscriptions)

    return add_new_id_to_subscriptions(cursor, subscriptions)


def handle_level_changes(subscription, venue_id, level_id = False):
    """ Updates Subscription Levels to the new ones """
    if level_id:
        return level_id
    # we have the luxury (!) of explicitly knowing which venue_ids level_ids map to which new ones
    if venue_id == 1:
        if subscription['level_id'] == 1:
            return 1
        elif subscription['level_id'] == 2:
            return 3
        elif subscription['level_id'] == 3:
            return 13
        elif subscription['level_id'] == 4:
            return 14
        elif subscription['level_id'] == 17:
            return 13
        else:
            return 15
    if venue_id == 2:
        if subscription['level_id'] == 1:
            return 7
        elif subscription['level_id'] == 2:
            return 7
        elif subscription['level_id'] == 3:
            return 13
        elif subscription['level_id'] == 4:
            return 14
        elif subscription['level_id'] == 5:
            return 8
        elif subscription['level_id'] == 6:
            return 8
        elif subscription['level_id'] == 7:
            return 9
        elif subscription['level_id'] == 9:
            return 13
        elif subscription['level_id'] == 31:
            return 11
        elif subscription['level_id'] == 34:
            return 13
        elif subscription['level_id'] == 35:
            return 7
        elif subscription['level_id'] == 36:
            return 7
        elif subscription['level_id'] == 37:
            return 8
        elif subscription['level_id'] == 40:
            return 14
        else:
            return 16


def handle_custom_field_mapping(cursor, subscriptions, custom_field_map, venue_id):
    """ Updates subscriptions custom data (addresses, etc) """

    temp = []
    for custom_field in custom_field_map:
        del custom_field['id']
        good = True
        custom_field['subscription_id'] = update_subscription_id(custom_field, subscriptions)
        # as with levels, we know which id should map to which
        if venue_id == 1:
            if custom_field['custom_field_id'] == 2:
                custom_field['custom_field_id'] = 1
            elif custom_field['custom_field_id'] == 3:
                custom_field['custom_field_id'] = 2
            elif custom_field['custom_field_id'] == 7:
                custom_field['custom_field_id'] = 3
            elif custom_field['custom_field_id'] == 8:
                custom_field['custom_field_id'] = 4
            elif custom_field['custom_field_id'] == 9:
                custom_field['custom_field_id'] = 5
            elif custom_field['custom_field_id'] == 10:
                custom_field['custom_field_id'] = 6
            elif custom_field['custom_field_id'] == 11:
                custom_field['custom_field_id'] = 7
            elif custom_field['custom_field_id'] == 26:
                custom_field['custom_field_id'] = 8
            elif custom_field['custom_field_id'] == 27:
                custom_field['custom_field_id'] = 9
        if venue_id == 2:
            if custom_field['custom_field_id'] == 2:
                custom_field['custom_field_id'] = 1
            elif custom_field['custom_field_id'] == 3:
                custom_field['custom_field_id'] = 2
            elif custom_field['custom_field_id'] == 7:
                custom_field['custom_field_id'] = 3
            elif custom_field['custom_field_id'] == 8:
                custom_field['custom_field_id'] = 4
            elif custom_field['custom_field_id'] == 9:
                custom_field['custom_field_id'] = 5
            elif custom_field['custom_field_id'] == 10:
                custom_field['custom_field_id'] = 6
            elif custom_field['custom_field_id'] == 11:
                custom_field['custom_field_id'] = 7
            elif custom_field['custom_field_id'] == 25:
                custom_field['custom_field_id'] = 8
            elif custom_field['custom_field_id'] == 27:
                custom_field['custom_field_id'] = 9
            elif custom_field['custom_field_id'] == 26:
                good = False
        if good:
            temp.append(custom_field)

    insert_many(cursor, 'mono_subscriptions_custom_field_map', temp)


def handle_cancellation_reports(cursor, subscriptions, cancellations, which_database):
    """ Handle previous cancellation reasons """
    for cancellation in cancellations:
        del cancellation['id']
        cancellation['subscription_id'] = update_subscription_id(cancellation, subscriptions)
        data = json.loads(cancellation['data'])
        cancellation_data = {}
        # convert different cancellation data in to the same form
        for key, value in data.items():
            if key == 'reason':
                if value == 'leaving-area':
                    value == 'moving-out-area'
            if key == 'how-likely-are-you-recommend-us-friend':
                key = 'how-likely-are-you-recommend-workout-bristol-friend-1-not-likely-10-very-likely'
                if value == '1-not-likely':
                    value = 1
                if value == '10-very-likely':
                    value = 10
            if key == 'do-you-have-any-suggestions-how-we-could-improve':
                key = 'do-you-have-any-suggestions-how-we-could-improve-workout-bristol'
            cancellation_data[key] = value
        cancellation['data'] = json.dumps(cancellation_data)

    insert_many(cursor, 'mono_subscriptions_cancellations', cancellations)


def handle_subscriptions_extras(cursor, subscriptions, transactions, ref_history, actions, marketing_preferences):
    """ Insert all subscription extra details """

    # update all records with new subscription ids
    for transaction in transactions:
        del transaction['id']
        transaction['subscription_id'] = update_subscription_id(transaction, subscriptions)
    for reference in ref_history:
        del reference['id']
        reference['subscription_id'] = update_subscription_id(reference, subscriptions)
    for action in actions:
        del action['id']
        action['subscription_id'] = update_subscription_id(action, subscriptions)
    for marketing_preference in marketing_preferences:
        marketing_preference['subscription_id'] = update_subscription_id(marketing_preference, subscriptions)

    insert_many(cursor, 'mono_subscriptions_transactions', transactions)
    insert_many(cursor, 'mono_subscriptions_ref_history', ref_history)
    insert_many(cursor, 'mono_subscriptions_actions', actions)
    insert_many(cursor, 'mono_company_subscriptions_categories', marketing_preferences)


def add_new_id_to_subscriptions(cursor, old_subscriptions):
    """ add the new subscription ids to the original subscriptions object """
    cursor.execute("SELECT * FROM mono_subscriptions")
    new_subscriptions = cursor.fetchall()

    for subscription in old_subscriptions:
        for new_subscription in new_subscriptions:
            if subscription['email'].lower() == new_subscription['email'].lower():
                subscription['new_id'] = new_subscription['id']
                break
    
    return old_subscriptions


def update_subscription_id(item, subscriptions):
    """ Updates an item's subscription_id to the matching new ID """
    for subscription in subscriptions:
        if item['subscription_id'] == subscription['id']:
            return subscription['new_id']


def merge_events(master, to_merge, which_database, subscriptions = False):
    """ Merge events and event bookings """
    try:

        # get from old
        with to_merge.cursor() as cursor:

            print("Reading Event Data")

            if not subscriptions:
                cursor.execute("SELECT * FROM mono_subscriptions")
                subs = cursor.fetchall()

            # get events
            cursor.execute("SELECT * FROM mono_events")
            events = cursor.fetchall()
            # get registrants
            cursor.execute("SELECT * FROM mono_events_registrants")
            registrants = cursor.fetchall()
            # get registrants archive
            cursor.execute("SELECT * FROM mono_events_registrants_archive")
            registrants_archive = cursor.fetchall()
            # get waitinglists
            cursor.execute("SELECT * FROM mono_events_waitinglist")
            waitinglists = cursor.fetchall()
            # get waitinglists archive
            cursor.execute("SELECT * FROM mono_events_waitinglist_archive")
            waitinglists_archive = cursor.fetchall()
            # get special bookings
            cursor.execute("SELECT * FROM mono_events_special_bookings")
            special_bookings = cursor.fetchall()

        # put in to new
        with master.cursor() as cursor:

            print("Inserting Events")
            events = handle_events(cursor, events, which_database)

            # if anything in the user migration has failed (duplicate attempts)
            # get the list of subscriptions
            if not subscriptions:
                subscriptions = add_new_id_to_subscriptions(cursor, subs)

            if special_bookings:
                print("Inserting special bookings")
                for special_booking in special_bookings:
                    del special_booking['id']
                    special_booking['subscription_id'] = update_subscription_id(special_booking, subscriptions)
                    special_booking['event_id'] = update_event_id(special_booking, events)            
                insert_many(cursor, 'mono_events_special_bookings', special_bookings)

            if registrants:
                print("Inserting registrants")
                for registrant in registrants:
                    del registrant['id']
                    registrant['subscription_id'] = update_subscription_id(registrant, subscriptions)
                    registrant['event_id'] = update_event_id(registrant, events)
                insert_many(cursor, 'mono_events_registrants', registrants)
            if registrants_archive:
                print("Inserting registrants archive")
                for registrant in registrants_archive:
                    del registrant['id']
                    registrant['subscription_id'] = update_subscription_id(registrant, subscriptions)
                    registrant['event_id'] = update_event_id(registrant, events)
                insert_many(cursor, 'mono_events_registrants_archive', registrants_archive)

            if waitinglists:
                print("Inserting waitinglists")
                for waitinglist in waitinglists:
                    del waitinglist['id']
                    waitinglist['subscription_id'] = update_subscription_id(waitinglist, subscriptions)
                    waitinglist['event_id'] = update_event_id(waitinglist, events)
                insert_many(cursor, 'mono_events_waitinglist', waitinglists)
            if waitinglists_archive:
                print("Inserting waitinglists archive")
                for waitinglist in waitinglists_archive:
                    del waitinglist['id']
                    waitinglist['subscription_id'] = update_subscription_id(waitinglist, subscriptions)
                    waitinglist['event_id'] = update_event_id(waitinglist, events)
                insert_many(cursor, 'mono_events_waitinglist_archive', waitinglists_archive)

            master.commit()

    except Exception as exception:
        print("Merge Events error: " + str(exception))
        traceback.print_exc()


def handle_events(cursor, events, venue_id):
    """ Migrate events """

    # as events have no distinguishing features (unlike subscription/users email addresses)
    # we will have to do this another way, mainly by using autoincrement id
    cursor.execute("SHOW TABLE STATUS WHERE name = 'mono_events'")
    auto_increment = cursor.fetchone()
    auto_increment = auto_increment.get("Auto_increment")

    # update all events to use their new IDs and store the old one for later tallying
    temp = []
    for event in events:
        event['old_id'] = event['id']
        event['id'] = auto_increment
        event['master_venue_id'] = venue_id
        auto_increment += 1
        # update location
        if venue_id == 2:
            event['location_id'] = 3
        temp.append(event.copy())

    insert_many(cursor, 'mono_events', temp)

    # add the event/level relationship
    for event in events:
        if venue_id == 1:
            events_levels = [
                { 'event_id': event['id'], 'level_id': 1 },
                { 'event_id': event['id'], 'level_id': 2 },
                { 'event_id': event['id'], 'level_id': 3 },
                { 'event_id': event['id'], 'level_id': 4 },
                { 'event_id': event['id'], 'level_id': 5 },
                { 'event_id': event['id'], 'level_id': 6 },
                { 'event_id': event['id'], 'level_id': 7 },
                { 'event_id': event['id'], 'level_id': 8 },
                { 'event_id': event['id'], 'level_id': 9 },
                { 'event_id': event['id'], 'level_id': 10 },
                { 'event_id': event['id'], 'level_id': 13 },
                { 'event_id': event['id'], 'level_id': 14 },
                { 'event_id': event['id'], 'level_id': 15 }
            ]
            insert_many(cursor, 'mono_events_levels', events_levels)
        if venue_id == 2:
            events_levels = [
                { 'event_id': event['id'], 'level_id': 5 },
                { 'event_id': event['id'], 'level_id': 6 },
                { 'event_id': event['id'], 'level_id': 7 },
                { 'event_id': event['id'], 'level_id': 8 },
                { 'event_id': event['id'], 'level_id': 9 },
                { 'event_id': event['id'], 'level_id': 10 },
                { 'event_id': event['id'], 'level_id': 11 },
                { 'event_id': event['id'], 'level_id': 12 },
                { 'event_id': event['id'], 'level_id': 13 },
                { 'event_id': event['id'], 'level_id': 14 },
                { 'event_id': event['id'], 'level_id': 16 }
            ]
            insert_many(cursor, 'mono_events_levels', events_levels)

    return events


def update_event_id(item, events):
    """ Updates an item's event_id to the matching new ID """
    for event in events:
        if item['event_id'] == event['old_id']:
            return event['id']


def merge_roombookings(master, to_merge, which_database, subscriptions = False):
    """ Merge Rooms and Bookings """
    try:

        # get from old
        with to_merge.cursor() as cursor:

            print("Reading Rooms")

            if not subscriptions:
                cursor.execute("SELECT * FROM mono_subscriptions")
                subs = cursor.fetchall()

            # get rooms
            cursor.execute("SELECT * FROM mono_roombooking_rooms")
            rooms = cursor.fetchall()
            # get exclusions
            cursor.execute("SELECT * FROM mono_roombooking_exclusions")
            exclusions = cursor.fetchall()
            # get bookings
            cursor.execute("SELECT * FROM mono_roombooking_bookings")
            bookings = cursor.fetchall()

        # put in to new
        with master.cursor() as cursor:

            print("Inserting Rooms")
            rooms = handle_rooms(cursor, rooms, which_database)

            # if anything in the user migration has failed (duplicate attempts)
            # get the list of subscriptions
            if not subscriptions:
                subscriptions = add_new_id_to_subscriptions(cursor, subs)

            if exclusions:
                print("Inserting exclusions")
                for exclusion in exclusions:
                    del exclusion['id']
                insert_many(cursor, 'mono_roombooking_exclusions', exclusions)

            if bookings:
                print("Inserting bookings")
                for booking in bookings:
                    del booking['id']
                    # update subscription id
                    booking['subscription_id'] = update_subscription_id(booking, subscriptions)
                    # update room id
                    for room in rooms:
                        if booking['room_id'] == room['old_id']:
                            booking['room_id'] = room['id']
                            break
                insert_many(cursor, 'mono_roombooking_bookings', bookings)

            master.commit()

    except Exception as exception:
        print("Merge Rooms error: " + str(exception))
        traceback.print_exc()


def handle_rooms(cursor, rooms, venue_id):
    """ Migrate rooms """

    # see handle_events for reasoning
    cursor.execute("SHOW TABLE STATUS WHERE name = 'mono_events'")
    auto_increment = cursor.fetchone()
    auto_increment = auto_increment.get("Auto_increment")

    # update all rooms to use their new IDs and store the old one for later tallying
    temp = []
    for room in rooms:
        room['old_id'] = room['id']
        room['id'] = auto_increment
        room['master_venue_id'] = venue_id
        auto_increment += 1
        temp.append(room.copy())

    insert_many(cursor, 'mono_roombooking_rooms', temp)

    # add specific allowable levels for rooms
    for room in rooms:
        rooms_levels = [
            { 'room_id': room['id'], 'level_id': 9 },
            { 'room_id': room['id'], 'level_id': 10 },
            { 'room_id': room['id'], 'level_id': 13 },
            { 'room_id': room['id'], 'level_id': 14 }
        ]
        insert_many(cursor, 'mono_roombooking_rooms_levels', rooms_levels)

    return rooms


def insert_many(cursor, table, items):
    """ Setup SQL and inserts many items """
    for item in items:
        if 'old_id' in item:
            del item['old_id']
    columns = ', ' . join(items[0].keys())
    values = ', ' . join('%s' for value in items[0].values())
    sql = 'INSERT INTO %s (%s) VALUES (%s)' % (table, columns, values)
    insert = [tuple([value for value in item.values()]) for item in items]
    cursor.executemany(sql, insert)


main()
